import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import axios from "axios";
import classes from "../Detail/detail.module.scss";
import { Button } from "@mui/material";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const DetailPage = () => {
  const [dataCountry, setDataCountry] = useState();
  const { countryName } = useParams();
  const navigate = useNavigate();

  const baseUrl = `https://restcountries.com/v3.1/name/${countryName.toLowerCase()}`;

  const fetchDetailCountry = async () => {
    try {
      const response = await axios.get(baseUrl);

      setDataCountry(response.data[0]);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchDetailCountry();
  }, [countryName]);

  if (!dataCountry) {
    return <div className={classes.loader}></div>;
  }

  if (dataCountry) {
    return (
      <>
        <div className={classes.detailContainer}>
          <div>
            <Button
              variant="outlined"
              startIcon={<ArrowBackIcon />}
              sx={{
                color: "black",
                border: "1px solid black",
                width: "150px",
              }}
              onClick={() => navigate(-1)}
            >
              Back
            </Button>
          </div>
          <div className={classes.detailContent}>
            <div>
              <img
                className={classes.detailImage}
                src={dataCountry?.flags.svg}
                alt="flags"
              />
            </div>
            <div className={classes.parent}>
              <div className={classes.detailName}>
                <h1>{dataCountry?.name.common}</h1>
              </div>
              <div className={classes.detailItem}>
                <div>
                  <strong>Native Name:</strong> {dataCountry?.name.official}
                </div>
                <div>
                  <strong>Population:</strong> {dataCountry?.population}
                </div>
                <div>
                  <strong>Region:</strong> {dataCountry?.region}
                </div>
                <div>
                  <strong>Sub Region:</strong> {dataCountry?.subregion}
                </div>
                <div>
                  <strong>Capital:</strong> {dataCountry?.capital[0]}
                </div>
              </div>
              <div className={classes.detailItem}>
                <div>
                  <strong>Top Level Domain:</strong> {dataCountry?.tld[0]}
                </div>
                <div>
                  <strong>Currencies:</strong>
                </div>
                <div>
                  <strong>Languages:</strong>
                </div>
              </div>
              <div className={classes.detailFooter}>
                <div>
                  <strong>Border Countries:</strong>
                </div>
                {dataCountry?.borders ? (
                  <div className={classes.bordersDetail}>
                    {dataCountry.borders.map((item) => {
                      return <div className={classes.bordersItem}>{item}</div>;
                    })}
                  </div>
                ) : (
                  "-"
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
};

export default DetailPage;
