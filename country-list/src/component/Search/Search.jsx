import React from "react";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import { IconButton } from "@mui/material";

export default function SearchBar({ onChange }) {
  return (
    <TextField
      placeholder="Search for a country..."
      variant="outlined"
      sx={{ m: 0, height: 56 }}
      onChange={(e) => onChange(e.target.value)}
      InputProps={{
        startAdornment: (
          <IconButton type="submit">
            <SearchIcon />
          </IconButton>
        ),
      }}
    />
  );
}
