import * as React from "react";
import classes from "../Navbar/navbar.module.scss";
import { Button } from "@mui/material";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import LightModeIcon from "@mui/icons-material/LightMode";

export default function ButtonAppBar({ darkMode, handleThemeToggle }) {
  return (
    <div className={`${classes.bgNavbar} ${darkMode ? classes.darkMode : ""}`}>
      <h2>Where in the world?</h2>
      <Button
        variant="contained"
        startIcon={darkMode ? <LightModeIcon /> : <DarkModeIcon />}
        onClick={handleThemeToggle}
        className={classes.button}
        sx={
          darkMode
            ? { backgroundColor: "hsl(209, 23%, 22%)", color: "white" }
            : { backgroundColor: "white", color: "hsl(209, 23%, 22%)" }
        }
      >
        {darkMode ? "Light Mode" : "Dark Mode"}
      </Button>
    </div>
  );
}
