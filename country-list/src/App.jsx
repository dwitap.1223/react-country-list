import { Route, Routes } from "react-router-dom";
import classes from "./style.module.scss";
import ButtonAppBar from "./component/Navbar/Navbar";
import Country from "./pages/HomePage/Home";
import DetailPage from "./pages/Detail/Detail";
import { useEffect, useState } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { CssBaseline } from "@mui/material";

function App() {
  const [darkMode, setDarkMode] = useState(false);

  const lightTheme = createTheme({
    palette: {
      primary: {
        main: "#ffffff",
      },
      secondary: {
        main: "#fafafa",
      },
      mode: "light",
    },
  });

  const darkTheme = createTheme({
    palette: {
      primary: {
        main: "#2b3945",
      },
      secondary: {
        main: "#202c37",
      },
      mode: "dark",
    },
  });

  useEffect(() => {
    const rootElement = document.getElementById("root");
    if (rootElement) {
      if (darkMode) {
        rootElement.classList.add("dark-theme");
      } else {
        rootElement.classList.remove("dark-theme");
      }
    }
  }, [darkMode]);

  const handleThemeToggle = () => {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  };

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      <div className={classes.mainContainer}>
        <ButtonAppBar
          darkMode={darkMode}
          handleThemeToggle={handleThemeToggle}
        />
        <Routes>
          <Route path="/" element={<Country />} />
          <Route path="/detail/:countryName" element={<DetailPage />} />
        </Routes>
      </div>
    </ThemeProvider>
  );
}

export default App;
