import { useEffect, useState } from "react";
import axios from "axios";
import classes from "../HomePage/home.module.scss";
import { Link } from "react-router-dom";
import SearchBar from "../../component/Search/Search";
import {
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  IconButton,
  MenuItem,
  Pagination,
  Select,
  Typography,
} from "@mui/material";
import SortRoundedIcon from "@mui/icons-material/SortRounded";

const Country = () => {
  const [country, setCountry] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [filter, setFilter] = useState([]);
  const [selectedRegion, setSelectedRegion] = useState("All");
  const [sort, setSort] = useState(true);
  const maxItemPage = 12;

  const baseUrl = "https://restcountries.com/v3.1/all";

  const fetchCountry = async () => {
    try {
      const response = await axios.get(baseUrl);

      setCountry(response.data);
    } catch (err) {
      console.log(err);
    }
  };

  const handleSearchChange = (val) => {
    const filteredResult = country.filter((item) =>
      item.name.common.toLowerCase().includes(val.toLowerCase())
    );

    setFilter(filteredResult);
    setCurrentPage(1);
  };

  const allData = filter.length > 0 ? filter : country;

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const handleRegionChange = (event) => {
    const selectedRegion = event.target.value;
    setSelectedRegion(selectedRegion);
    setCurrentPage(1);
  };

  const filteredByRegion =
    selectedRegion === "All"
      ? allData
      : allData.filter(
          (item) => item.region.toLowerCase() === selectedRegion.toLowerCase()
        );

  const handleSort = () => {
    if (sort === "asc") {
      const sortedData = [...allData].sort((a, b) =>
        a.name.common.localeCompare(b.name.common)
      );
      setFilter(sortedData);
      setSort("desc");
    } else {
      const sortedData = [...allData].sort((a, b) =>
        b.name.common.localeCompare(a.name.common)
      );
      setFilter(sortedData);
      setSort("asc");
    }
  };

  const lastIndex = currentPage * maxItemPage;
  const firstIndex = lastIndex - maxItemPage;
  const indexExist = filteredByRegion.slice(firstIndex, lastIndex);

  const renderCountry = () => {
    return indexExist?.map((item, i) => {
      return (
        <>
          <Link to={`/detail/${item.name.common}`}>
            <Card sx={{ maxWidth: "265px" }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="159px"
                  image={item.flags.svg}
                  alt={item.name.common}
                />
                <CardContent>
                  <Typography gutterBottom component="div">
                    <strong>{item.name.common}</strong>
                  </Typography>
                  <br />
                  <Typography gutterBottom component="div">
                    <strong>Population:</strong> {item.population}
                  </Typography>
                  <Typography gutterBottom component="div">
                    <strong>Region:</strong> {item.region}
                  </Typography>
                  <Typography gutterBottom component="div">
                    <strong>Capital:</strong> {item.capital}
                  </Typography>
                </CardContent>
              </CardActionArea>
            </Card>
          </Link>
        </>
      );
    });
  };

  useEffect(() => {
    fetchCountry();
  }, []);

  return (
    <>
      <div className={classes.content}>
        <SearchBar onChange={handleSearchChange} className={classes.search} />
        <div className={classes.bar}>
          <Select value={selectedRegion} onChange={handleRegionChange}>
            <MenuItem value="All">Filter By Region</MenuItem>
            <MenuItem value="Africa">Africa</MenuItem>
            <MenuItem value="Americas">America</MenuItem>
            <MenuItem value="Asia">Asia</MenuItem>
            <MenuItem value="Europe">Europe</MenuItem>
            <MenuItem value="Oceania">Oceania</MenuItem>
          </Select>
          <IconButton onClick={() => handleSort()}>
            <SortRoundedIcon
              color="inherit"
              style={sort === "asc" ? { transform: "rotateX(180deg)" } : null}
            />
          </IconButton>
        </div>
      </div>
      <div className={classes.list}>{renderCountry()}</div>
      <Pagination
        count={Math.ceil(allData.length / maxItemPage)}
        page={currentPage}
        onChange={handlePageChange}
        shape="rounded"
        sx={{ pb: 5 }}
      />
    </>
  );
};

export default Country;
